# Tasker Agent

## Objective

Create a simple task scheduler using an agent client and a server task controller.

## Functional Specifications

Create a agent / server application, which will execute scheduled tasks in client side.

## Technical Specifications

The following list of technical specifications should be adhered to:

- For the web application

    - Create a database with any required tables.
    - Create a  web REST application.
    - Create a website which will show the following data from servers scheduled tasks:

        - Start date and time
        - Execution time
        - Stop date and time
        - Process executed

    - Website should be configurable, you must include a CRUD to schedule tasks, using the fields:

        - Task name
        - Executable path
        - Start date and time
        - End date and time
        - Days of the week to be executed.
        - Server which will execute the task. (When installing the agent, it will provide a key to the server, this key will link server-task)

    - Website will show a log of  the tasks executed and failed.
    - Apply input validations and constraints wherever necessary to create a stable application.
    - Create an Agent client which will be installed and should run automatically in client side.

- For the web service
    - Create a secured REST web service.

- The application and services should not be dependent on each other in any way.

- Even if you are not able to complete all the tasks, try to achieve a working application and service.
