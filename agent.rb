class Agent
  require 'securerandom'
  require './task'

  attr_accessor :key

  def initialize(port)

    self.key= SecureRandom.hex
    puts "Hello I'm your new Task Agent, this is my private key: #{self.key}"
    puts 'Use it to schedule task on the scheduler server.'
    puts '================================================'
    puts 'These are all the available tasks: '
    Task.AVAILABLE.each { |t| puts "- #{t.name}: #{t.description}" }
    puts "Send the Task name as a POST parameter 'task_name'"
    puts '================================================'
    puts "This is my trigger URI: http://localhost:#{port}/trigger"
    puts "This is my private key: #{self.key}"

  end

  # Starts the available task execution
  def trigger(task_name, execution_path = nil)
    task = Task.AVAILABLE.find { |s| s.name == task_name }
    task.run execution_path unless task.nil?
  end

end