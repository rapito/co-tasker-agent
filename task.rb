class Task
  # available tasks array
  @@AVAILABLE = []

  def self.initialize_available
    @@AVAILABLE = [HelloTask.new, FileListTask.new]
  end

  def self.AVAILABLE
    @@AVAILABLE
  end

  attr_accessor :name
  attr_accessor :description
  attr_accessor :start_time

  def initialize
    self.name = self.class.name
  end

  # Executes the task at the specified path
  def run path
    pre_run
    execute_task path
    post_run
  end

  # method to execute when triggered
  def execute_task path
    puts "Empty run method on path: #{path}"
  end

  def post_run
    puts "Finished Task #{self.name}, lasted #{Time.now - self.start_time} seconds."
  end

  def pre_run
    self.start_time = Time.now
    puts "Starting Task #{self.name}: #{self.description}"
  end

end

class HelloTask < Task
  def initialize
    super
    @description = "Prints a simple 'Hello Task' message into the console."
  end

  def execute_task path
    puts 'Hello Task'
  end
end

class FileListTask < Task
  def initialize
    super
    @description = 'Prints all files available at the executed directory.'
  end

  def execute_task path
    Dir.entries(path).each { |p| puts p }
  end
end

Task.initialize_available
