require 'sinatra'
require './agent'

port = [*4567...7890].sample
# port = 4567
set :port, port

agent = Agent.new port

def base_url
  @base_url ||= "#{request.env['rack.url_scheme']}://#{request.env['HTTP_HOST']}"
end

def check_key(agent, key)
  error 401, 'invalid agent_key' unless key == agent.key
end

post '/trigger' do
  task_name = params['task_name']
  execution_path = params['execution_path']
  check_key agent, params['agent_key']

  agent.trigger task_name, execution_path unless task_name.nil?
end

get '/' do
  check_key agent, params['agent_key']

  agent.trigger 'HelloTask'
  # agent.trigger 'FileListTask', "C:\\"
end

